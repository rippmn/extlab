# Extra Lab Steps

The following Steps are intended to be done after the completion of the [Deploy Your Website on Cloud Run](https://www.cloudskillsboost.google/catalog_lab/2487) Cloud Skills Boost Lab. These are additional steps intended to enhance your understanding of the capabilities of Cloud Run. 

During the lab the commands you executed used a specific region when deploying to Cloud Run. You must continue to use that region in these steps. To make that easier all of the following will leverage the REGION environment variable that you will set in Cloud Shell.


Before executing the below replace `<<YOUR_LABS_REGION>>` with the region you used in the Cloud Skills Boost Lab.

```
export REGION=<<YOUR_LABS_REGION>>
```
## Deploy from Source Code
The following steps will demonstrate deploying directly from source code to Cloud Run. They will leverage a simple Java Spring Boot application to do so. This can be done with any of the [Cloud Run Buildpacks](https://cloud.google.com/docs/buildpacks/builders) languages.


Clone the simple Java Spring-boot project that is built with Apache Maven.

```
git clone https://gitlab.com/rippmn/cr-java.git
cd cr-java
```

Look at the contents of the project.toml file (these configure the build settings for the build pack) For more configurable details see the details at [Configure Cloud Run and Cloud Functions services](https://cloud.google.com/docs/buildpacks/service-specific-configs). In this example we only need to set the Java Version

```
cat project.toml
```

This configuration tells the build pack to use Java 17 now required by Spring Boot.

```[[build.env]]
name =  "GOOGLE_RUNTIME_VERSION"
value = "17"
```

Now deploy from the source code. The ```source``` tag tells gcloud to upload the source code. (Use the same REGION you used earlier in the lab)

```
gcloud run deploy java-hello --source . --region $REGION --allow-unauthenticated
```

You will be prompted to create an artifact registry repository. Input 'Y' to continue.

```
Deploying from source requires an Artifact Registry Docker repository to store built containers. A repository named [cloud-run-source-deploy] in region [us-central1] will be created.

Do you want to continue (Y/n)?  
```

In a couple of minutes the web application will be deployed and serving traffic. Launch the service by using the link returned from the command line.

### Blue Green Canary rollout

Deploy blue version of the app

```
gcloud run deploy hello --image us-central1-docker.pkg.dev/ripka-labs/labs/hello-bg-app:v1 --allow-unauthenticated --region $REGION
```

Now deploy a new "green" revision by using and setup a unique canary revision URL (tag)

```
gcloud run deploy hello --image us-central1-docker.pkg.dev/ripka-labs/labs/hello-bg-app:v1  --allow-unauthenticated  --region $REGION --no-traffic --tag green --set-env-vars COLOR=green
```

Go to the cloud console for Cloud Run and click on the hello service. Once there click on the URL to launch the blue version in a new tab.

![Cloud Run URL](img/primary-url.png){width=50%}

From the Revisions tab now launch the green "canary"  version by clicking on the "tag" link which links to the unique canary URL. The Green version will open. 

![Cloud Run Tag URL](img/green-url.png){width=50%}

Close the tab with the Green Version This unique URL can  click on the link to open the canary revision in the browser. 

**Traffic Shifting**

Change the traffic percentages to allocate 50% of the traffic to the green version.

```
gcloud run services update-traffic hello --to-tags canary=50 --region $REGION
```

Open refresh (or reopen) the primary application link. Notice that upon multiple refreshes you start to see the server revision switch between blue and green.

Now allocate 100% of the traffic to the green revision.

```
gcloud run services update-traffic hello --to-tags canary=100 --region $REGION
```

When you refresh now you will only see the green revision.